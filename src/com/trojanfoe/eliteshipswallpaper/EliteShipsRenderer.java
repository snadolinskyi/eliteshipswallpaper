/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.eliteshipswallpaper;

import android.opengl.*;
import android.util.*;
import com.trojanfoe.opengl.GLWallpaperService;
import com.trojanfoe.util.Timer;
import javax.microedition.khronos.egl.*;
import javax.microedition.khronos.opengles.*;

class EliteShipsRenderer implements GLWallpaperService.Renderer
{
    protected static final String TAG = "EliteShipsRenderer";
    protected ShipObject mShip;
    protected float mScreenWidth, mScreenHeight;
    protected float mXOffset = 0.0f;
    protected float mXTouch = -1.0f;
    protected float mYTouch = -1.0f;
    protected boolean mVisible = true;
    // Debugging
    protected static final Timer mTimer = null; // new Timer();

    public EliteShipsRenderer()
    {
        mShip = new ShipObject();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // Log.d(TAG, "onSurfaceCreated()");

        mShip = new ShipObject();

        float[] pos0 =
        {
            50.0f, 50.0f, 100.0f, 0.0f
        };
        float[] pos1 =
        {
            -50.0f, -50.0f, 20.0f, 0.0f
        };

        float[] ambient =
        {
            0.8f, 0.8f, 0.8f, 1.0f
        };
        float[] diffuse =
        {
            0.8f, 0.8f, 0.8f, 1.0f
        };
        float[] specular =
        {
            1.0f, 1.0f, 1.0f, 1.0f
        };

        gl.glShadeModel(GL10.GL_FLAT);

        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClearDepthf(1.0f);
        gl.glEnable(GL10.GL_CULL_FACE);
        gl.glCullFace(GL10.GL_BACK);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glDepthFunc(GL10.GL_LEQUAL);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);

        gl.glEnable(GL10.GL_LIGHTING);
        gl.glEnable(GL10.GL_LIGHT0);
        gl.glEnable(GL10.GL_LIGHT1);

        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, pos0, 0);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPECULAR, specular, 0);

        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_POSITION, pos1, 0);
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_SPECULAR, specular, 0);

        mShip.init(gl);
    }

    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        // Log.d(TAG, "onSurfaceChanged(width=" + width + ", height=" + height +
        // ")");

        mScreenWidth = (float) width;
        mScreenHeight = (float) height;

        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();

        gl.glViewport(0, 0, width, height);

        float aspect = (float) width / (float) height;
        if (aspect < 0.0f)
        {
            aspect = -aspect;
        }
        GLU.gluPerspective(gl, 45.0f, aspect, 1.0f, 15000.0f);

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void onDrawFrame(GL10 gl)
    {
        // Log.d(TAG, "onDrawFrame()");

        if (mTimer != null)
        {
            mTimer.startOperation();
        }

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();

        GLU.gluLookAt(gl, mXOffset, 0.0f, 60.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

        // This doesn't work very well - pressing down too long brings-up a
        // dialog box and lighting
        // doesn't appear very well....
        // if (mXTouch >= 0.0f && mYTouch >= 0.0f)
        // {
        // gl.glEnable(GL10.GL_LIGHT1);
        // pos1[0] = mXTouch - (mScreenWidth / 2.0f);
        // pos1[1] = mYTouch - (mScreenHeight / 2.0f);
        // gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_POSITION, pos1, 0);
        // }
        // else
        // {
        // gl.glDisable(GL10.GL_LIGHT1);
        // }

        mShip.animate();

        mShip.draw(gl);

        if (mTimer != null)
        {
            mTimer.endOperation();
            if ((mTimer.getTotalCounter() % 100) == 0)
            {
                Log.d(TAG, mTimer.toString());
            }
        }
    }

    public void setXOffset(float xOffset)
    {
        // Input offset will go from 0.0 (left) to 1.0 (right). Make it so
        // offset runs from +0.5 (left) to -0.5 (right). Unless we are in
        // preview mode, where the offset is always 0.0
        mXOffset = -(xOffset - 0.5f) * 30.0f;
        // Log.d(TAG, "New X offset=" + mXOffset + " (input=" + mXOffset + ")");
    }

    public void setVisible(boolean visible)
    {
        // Log.d(TAG, "New visibility=" + mVisible);
        mVisible = visible;
    }

    public void setTouch(float xTouch, float yTouch)
    {
        // Log.d(TAG, "Touch x=" + mXTouch + ", y=" + mYTouch);
        mXTouch = xTouch;
        mYTouch = yTouch;
    }

    public void setNoTouch()
    {
        // Log.d(TAG, "No touch");
        mXTouch = -1.0f;
        mYTouch = -1.0f;
    }

    public void setDrawMode(int drawMode)
    {
        mShip.setDrawMode(drawMode);
    }

    public void setRandomiseShipModel(boolean randomiseShipModel)
    {
        mShip.setRandomizeShipMode(randomiseShipModel);
    }
}

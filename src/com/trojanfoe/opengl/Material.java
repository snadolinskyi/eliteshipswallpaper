/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.opengl;

import javax.microedition.khronos.opengles.*;

public class Material
{
    protected String mName;

    protected float[] mAmbient;

    protected float[] mDiffuse;

    protected float[] mSpecular;

    protected float[] mShininess;

    protected float mAlpha;

    public Material(String name, Vector3f ambient, Vector3f diffuse, Vector3f specular, float shininess, float alpha)
    {
        mName = name;
        mAmbient = new float[4];
        ambient.copyTo(mAmbient, 0);
        mAmbient[3] = 1.0f;

        mDiffuse = new float[4];
        diffuse.copyTo(mDiffuse, 0);
        mDiffuse[3] = 1.0f;

        mSpecular = new float[4];
        specular.copyTo(mSpecular, 0);
        mSpecular[3] = 1.0f;

        mShininess = new float[1];
        mShininess[0] = shininess;
        mAlpha = alpha;
    }

    public void set(GL10 gl)
    {
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, mAmbient, 0);
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, mDiffuse, 0);
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, mSpecular, 0);
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, mShininess, 0);
    }

    public void setName(String name)
    {
        mName = name;
    }

    public String getName()
    {
        return mName;
    }

    public void setAmbient(Vector3f ambient)
    {
        ambient.copyTo(mAmbient, 0);
    }

    public float[] getAmbient()
    {
        return mAmbient;
    }

    public void setDiffuse(Vector3f diffuse)
    {
        diffuse.copyTo(mDiffuse, 0);
    }

    public float[] getDiffuse()
    {
        return mDiffuse;
    }

    public void setSpecular(Vector3f specular)
    {
        specular.copyTo(mSpecular, 0);
    }

    public float[] getSpecular()
    {
        return mSpecular;
    }

    public void setShininess(float shininess)
    {
        this.mShininess[0] = shininess;
    }

    public float getShininess()
    {
        return mShininess[0];
    }

    public void setAlpha(float alpha)
    {
        mAlpha = alpha;
    }

    public float getAlpha()
    {
        return mAlpha;
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Cobramk3 extends Model
{
    public Cobramk3()
    {
        super("Cobramk3");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.151612f,
                0.099691f, 0.493430f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.224112f,
                0.109070f, 0.610881f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.347526f,
                0.470611f, 0.709540f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.001", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.080754f,
                0.053750f, 0.258528f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.471656f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.287147f,
                0.409214f, 0.648465f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.007", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f,
                0.112000f, 0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[5]), new Face(2, 1, 3, 1, 1, 1, mMaterials[1]),
            new Face(2, 3, 4, 1, 1, 1, mMaterials[1]), new Face(5, 0, 2, 2, 2, 2, mMaterials[2]),
            new Face(2, 6, 5, 3, 3, 3, mMaterials[0]), new Face(2, 4, 7, 4, 4, 4, mMaterials[2]),
            new Face(2, 7, 6, 5, 5, 5, mMaterials[5]), new Face(1, 0, 8, 6, 6, 6, mMaterials[2]),
            new Face(8, 9, 1, 7, 7, 7, mMaterials[0]), new Face(1, 9, 10, 8, 8, 8, mMaterials[5]),
            new Face(1, 10, 3, 9, 9, 9, mMaterials[2]), new Face(8, 0, 11, 10, 10, 10, mMaterials[1]),
            new Face(0, 5, 11, 11, 11, 11, mMaterials[1]), new Face(8, 10, 9, 12, 12, 12, mMaterials[3]),
            new Face(7, 5, 6, 13, 13, 13, mMaterials[3]), new Face(12, 13, 14, 14, 14, 14, mMaterials[4]),
            new Face(12, 14, 15, 14, 14, 14, mMaterials[4]), new Face(16, 17, 18, 14, 14, 14, mMaterials[4]),
            new Face(16, 18, 19, 14, 14, 14, mMaterials[4]), new Face(20, 21, 22, 14, 14, 14, mMaterials[4]),
            new Face(23, 24, 25, 14, 14, 14, mMaterials[4]), new Face(26, 27, 28, 15, 15, 15, mMaterials[6]),
            new Face(26, 28, 29, 15, 15, 15, mMaterials[6]), new Face(30, 27, 28, 16, 16, 16, mMaterials[6]),
            new Face(30, 28, 31, 16, 16, 16, mMaterials[6]), new Face(32, 31, 30, 17, 17, 17, mMaterials[6]),
            new Face(32, 30, 33, 17, 17, 17, mMaterials[6]), new Face(26, 33, 32, 18, 18, 18, mMaterials[6]),
            new Face(26, 32, 29, 18, 18, 18, mMaterials[6]), new Face(5, 7, 21, 14, 14, 14, mMaterials[6]),
            new Face(7, 22, 21, 14, 14, 14, mMaterials[6]), new Face(4, 22, 7, 14, 14, 14, mMaterials[6]),
            new Face(4, 14, 22, 14, 14, 14, mMaterials[6]), new Face(13, 22, 14, 14, 14, 14, mMaterials[6]),
            new Face(13, 20, 22, 14, 14, 14, mMaterials[6]), new Face(5, 21, 20, 14, 14, 14, mMaterials[6]),
            new Face(5, 20, 13, 14, 14, 14, mMaterials[6]), new Face(5, 13, 12, 14, 14, 14, mMaterials[6]),
            new Face(5, 12, 11, 14, 14, 14, mMaterials[6]), new Face(11, 12, 17, 14, 14, 14, mMaterials[6]),
            new Face(4, 15, 14, 14, 14, 14, mMaterials[6]), new Face(3, 15, 4, 14, 14, 14, mMaterials[6]),
            new Face(3, 18, 15, 14, 14, 14, mMaterials[6]), new Face(12, 15, 18, 14, 14, 14, mMaterials[6]),
            new Face(12, 18, 17, 14, 14, 14, mMaterials[6]), new Face(8, 11, 17, 14, 14, 14, mMaterials[6]),
            new Face(8, 17, 16, 14, 14, 14, mMaterials[6]), new Face(8, 16, 24, 14, 14, 14, mMaterials[6]),
            new Face(8, 24, 23, 14, 14, 14, mMaterials[6]), new Face(16, 25, 24, 14, 14, 14, mMaterials[6]),
            new Face(16, 19, 25, 14, 14, 14, mMaterials[6]), new Face(3, 19, 18, 14, 14, 14, mMaterials[6]),
            new Face(3, 25, 19, 14, 14, 14, mMaterials[6]), new Face(3, 10, 25, 14, 14, 14, mMaterials[6]),
            new Face(10, 23, 25, 14, 14, 14, mMaterials[6]), new Face(8, 23, 10, 14, 14, 14, mMaterials[6]),
            new Face(26, 33, 30, 14, 14, 14, mMaterials[6]), new Face(26, 30, 27, 14, 14, 14, mMaterials[6]),
            new Face(38, 39, 41, 14, 14, 14, mMaterials[6]), new Face(38, 41, 40, 14, 14, 14, mMaterials[6]),
            new Face(34, 35, 41, 16, 16, 16, mMaterials[6]), new Face(34, 41, 39, 16, 16, 16, mMaterials[6]),
            new Face(35, 36, 40, 17, 17, 17, mMaterials[6]), new Face(35, 40, 41, 17, 17, 17, mMaterials[6]),
            new Face(36, 37, 38, 18, 18, 18, mMaterials[6]), new Face(36, 38, 40, 18, 18, 18, mMaterials[6]),
            new Face(37, 34, 39, 19, 19, 19, mMaterials[6]), new Face(37, 39, 38, 19, 19, 19, mMaterials[6]),
            new Face(28, 29, 34, 20, 20, 20, mMaterials[6]), new Face(28, 34, 37, 20, 20, 20, mMaterials[6]),
            new Face(31, 28, 37, 20, 20, 20, mMaterials[6]), new Face(31, 37, 36, 20, 20, 20, mMaterials[6]),
            new Face(32, 31, 36, 20, 20, 20, mMaterials[6]), new Face(32, 36, 35, 20, 20, 20, mMaterials[6]),
            new Face(29, 32, 35, 20, 20, 20, mMaterials[6]), new Face(29, 35, 34, 20, 20, 20, mMaterials[6])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(0f, 3.17823f, 3.08706f), new Vector3f(-4f, -0.07176f, 9.58706f),
            new Vector3f(4f, -0.07176f, 9.58706f), new Vector3f(-4f, -3.07177f, -4.94627f),
            new Vector3f(4f, -3.07177f, -4.94627f), new Vector3f(11f, 1.92824f, -4.94627f),
            new Vector3f(15f, -0.45176f, -0.91294f), new Vector3f(16f, -1.07176f, -4.94627f),
            new Vector3f(-11f, 1.92824f, -4.94627f), new Vector3f(-15f, -0.45176f, -0.91294f),
            new Vector3f(-16f, -1.07176f, -4.94627f), new Vector3f(0f, 3.17823f, -4.94627f),
            new Vector3f(1f, 1.42824f, -4.94627f), new Vector3f(4.5f, 0.92824f, -4.94627f),
            new Vector3f(4.5f, -1.57176f, -4.94627f), new Vector3f(1f, -2.07176f, -4.94627f),
            new Vector3f(-4.5f, 0.92824f, -4.94627f), new Vector3f(-1f, 1.42824f, -4.94627f),
            new Vector3f(-1f, -2.07176f, -4.94627f), new Vector3f(-4.5f, -1.57176f, -4.94627f),
            new Vector3f(10f, 0.67824f, -4.94627f), new Vector3f(11f, -0.07176f, -4.94627f),
            new Vector3f(10f, -0.82176f, -4.94627f), new Vector3f(-11f, -0.07176f, -4.94627f),
            new Vector3f(-10f, 0.67824f, -4.94627f), new Vector3f(-10f, -0.82176f, -4.94627f),
            new Vector3f(-0.2f, -0.27177f, 11.33706f), new Vector3f(0.2f, -0.27177f, 11.33706f),
            new Vector3f(0.2f, -0.27177f, 9.58706f), new Vector3f(-0.2f, -0.27177f, 9.58706f),
            new Vector3f(0.2f, 0.12824f, 11.33706f), new Vector3f(0.2f, 0.12824f, 9.58706f),
            new Vector3f(-0.2f, 0.12824f, 9.58706f), new Vector3f(-0.2f, 0.12824f, 11.33706f),
            new Vector3f(-0.2f, -0.27177f, 9.58706f), new Vector3f(-0.2f, 0.12824f, 9.58706f),
            new Vector3f(0.2f, 0.12824f, 9.58706f), new Vector3f(0.2f, -0.27177f, 9.58706f),
            new Vector3f(0.2f, -0.27177f, 9.59046f), new Vector3f(-0.2f, -0.27177f, 9.59046f),
            new Vector3f(0.2f, 0.12824f, 9.59046f), new Vector3f(-0.2f, 0.12824f, 9.59046f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, 0.894427f, 0.447214f), new Vector3f(0f, -0.979352f, 0.20216f),
            new Vector3f(0.301907f, 0.913953f, 0.271187f), new Vector3f(0.283203f, 0.922205f, 0.263314f),
            new Vector3f(0.161094f, -0.966561f, 0.19952f), new Vector3f(0.142899f, -0.972314f, 0.184893f),
            new Vector3f(-0.301907f, 0.913953f, 0.271187f), new Vector3f(-0.283203f, 0.922205f, 0.263314f),
            new Vector3f(-0.142899f, -0.972314f, 0.184893f), new Vector3f(-0.161094f, -0.966561f, 0.19952f),
            new Vector3f(-0.11291f, 0.993605f, -0f), new Vector3f(0.11291f, 0.993605f, -0f),
            new Vector3f(-0.514491f, 0.857485f, -0.004252f), new Vector3f(0.514491f, 0.857485f, -0.004252f),
            new Vector3f(0f, 0f, -1f), new Vector3f(0f, 1f, -0f), new Vector3f(1f, 0f, 0f), new Vector3f(0f, -1f, 0f),
            new Vector3f(-1f, 0f, 0f), new Vector3f(0f, 1f, -5e-006f), new Vector3f(0f, 0f, 0f)
        };
        return narr;
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.opengl;

import javax.microedition.khronos.egl.*;

class DefaultWindowSurfaceFactory implements EGLWindowSurfaceFactory
{
    public EGLSurface createWindowSurface(EGL10 egl, EGLDisplay display, EGLConfig config, Object nativeWindow)
    {
        // this is a bit of a hack to work around Droid init problems - if you
        // don't have this, it'll get hung up on orientation changes
        EGLSurface eglSurface = null;
        while (eglSurface == null)
        {
            try
            {
                eglSurface = egl.eglCreateWindowSurface(display, config, nativeWindow, null);
            }
            catch (Throwable t)
            {
            }
            finally
            {
                if (eglSurface == null)
                {
                    try
                    {
                        Thread.sleep(10);
                    }
                    catch (InterruptedException t)
                    {
                    }
                }
            }
        }
        return eglSurface;
    }

    public void destroySurface(EGL10 egl, EGLDisplay display, EGLSurface surface)
    {
        egl.eglDestroySurface(display, surface);
    }
}

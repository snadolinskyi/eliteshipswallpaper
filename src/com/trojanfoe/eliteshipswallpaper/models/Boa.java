/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Boa extends Model
{
    public Boa()
    {
        super("Boa");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.389446f,
                0.342241f, 0.649077f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.063356f, 0.749017f,
                0.079556f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.037600f, 0.400000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.800000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.010191f,
                0.000000f, 0.560000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[4]), new Face(0, 2, 3, 1, 1, 1, mMaterials[4]),
            new Face(0, 3, 4, 2, 2, 2, mMaterials[4]), new Face(0, 4, 5, 3, 3, 3, mMaterials[4]),
            new Face(6, 0, 5, 4, 4, 4, mMaterials[4]), new Face(6, 5, 7, 4, 4, 4, mMaterials[4]),
            new Face(6, 7, 8, 4, 4, 4, mMaterials[4]), new Face(6, 8, 9, 4, 4, 4, mMaterials[4]),
            new Face(9, 10, 11, 5, 5, 5, mMaterials[4]), new Face(9, 11, 2, 6, 6, 6, mMaterials[4]),
            new Face(9, 2, 1, 7, 7, 7, mMaterials[4]), new Face(9, 1, 6, 8, 8, 8, mMaterials[4]),
            new Face(3, 2, 11, 9, 9, 9, mMaterials[2]), new Face(7, 5, 4, 10, 10, 10, mMaterials[2]),
            new Face(9, 8, 10, 11, 11, 11, mMaterials[2]), new Face(3, 11, 12, 12, 12, 12, mMaterials[3]),
            new Face(10, 8, 12, 13, 13, 13, mMaterials[3]), new Face(7, 4, 12, 14, 14, 14, mMaterials[3]),
            new Face(12, 11, 10, 15, 15, 15, mMaterials[0]), new Face(12, 8, 7, 16, 16, 16, mMaterials[0]),
            new Face(12, 4, 3, 17, 17, 17, mMaterials[0]), new Face(1, 0, 6, 18, 18, 18, mMaterials[1])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(2.08f, 1.44f, 17.12f), new Vector3f(0f, -1.12f, 17.12f), new Vector3f(0f, -6.4f, 13.92f),
            new Vector3f(6.08f, -6.4f, 9.44f), new Vector3f(9.92f, 0f, 10.72f), new Vector3f(6.08f, 4f, 15.84f),
            new Vector3f(-2.08f, 1.44f, 17.12f), new Vector3f(3.84f, 10.4f, 12.64f),
            new Vector3f(-3.84f, 10.4f, 12.64f), new Vector3f(-6.08f, 4f, 15.84f), new Vector3f(-9.92f, 0f, 10.72f),
            new Vector3f(-6.08f, -6.4f, 9.44f), new Vector3f(0f, -0f, -14.88f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0.537803f, -0.436965f, 0.720992f), new Vector3f(0.533533f, -0.437092f, 0.724081f),
            new Vector3f(0.512254f, -0.453251f, 0.729493f), new Vector3f(0.517263f, -0.441707f, 0.733031f),
            new Vector3f(0f, 0.447214f, 0.894427f), new Vector3f(-0.507899f, -0.451466f, 0.733633f),
            new Vector3f(-0.531544f, -0.443927f, 0.721381f), new Vector3f(-0.52479f, -0.441195f, 0.727972f),
            new Vector3f(-0.505711f, -0.410891f, 0.758568f), new Vector3f(0f, -1f, 0f),
            new Vector3f(0.848419f, 0.442322f, 0.29075f), new Vector3f(-0.848419f, 0.442322f, 0.29075f),
            new Vector3f(0f, -0.967075f, -0.254493f), new Vector3f(-0.795383f, 0.521893f, -0.308211f),
            new Vector3f(0.795383f, 0.521893f, -0.308211f), new Vector3f(-0.838248f, -0.437985f, -0.324821f),
            new Vector3f(0f, 0.935432f, -0.353506f), new Vector3f(0.838248f, -0.437985f, -0.324821f),
            new Vector3f(-0f, 0f, 1f)
        };
        return narr;
    }
}

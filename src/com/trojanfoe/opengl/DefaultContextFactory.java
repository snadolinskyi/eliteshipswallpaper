/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.opengl;

import javax.microedition.khronos.egl.*;

class DefaultContextFactory implements EGLContextFactory
{
    public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig config)
    {
        return egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT, null);
    }

    public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context)
    {
        egl.eglDestroyContext(display, context);
    }
}

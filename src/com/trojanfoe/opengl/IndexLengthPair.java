/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.opengl;

public class IndexLengthPair
{
    public int mIndex;

    public int mLength;

    public IndexLengthPair()
    {
        mIndex = -1;
        mLength = -1;
    }

    public IndexLengthPair(int index, int length)
    {
        mIndex = index;
        mLength = length;
    }
}

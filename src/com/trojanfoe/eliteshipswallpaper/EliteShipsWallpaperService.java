/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.eliteshipswallpaper;

import android.content.*;
import android.util.*;
import android.view.*;
import com.trojanfoe.opengl.GLWallpaperService;
import com.trojanfoe.opengl.Model;

public class EliteShipsWallpaperService extends GLWallpaperService
{
    private static final String TAG = "EliteShipsWallpaperService";

    public static final String SHARED_PREFS_NAME = "eliteshipswallpaper";

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public Engine onCreateEngine()
    {
        return new EliteShipsEngine();
    }

    class EliteShipsEngine extends GLEngine implements SharedPreferences.OnSharedPreferenceChangeListener
    {
        protected EliteShipsRenderer mRenderer;

        protected SharedPreferences mPrefs;

        public EliteShipsEngine()
        {
            mRenderer = new EliteShipsRenderer();

            setRenderer(mRenderer);
            setRenderMode(RENDERMODE_CONTINUOUSLY);
            setTouchEventsEnabled(true);

            // Load preferences
            mPrefs = EliteShipsWallpaperService.this.getSharedPreferences(SHARED_PREFS_NAME, 0);
            mPrefs.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(mPrefs, null);
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels)
        {
            if (isPreview())
            {
                mRenderer.setXOffset(0.5f); // Otherwise 0.0 is sent
            }
            else
            {
                mRenderer.setXOffset(xOffset);
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible)
        {
            mRenderer.setVisible(visible);
        }

        @Override
        public void onTouchEvent(MotionEvent event)
        {
            if (event.getAction() == MotionEvent.ACTION_MOVE)
            {
                mRenderer.setTouch(event.getX(), event.getY());
            }
            else
            {
                mRenderer.setNoTouch();
            }
            super.onTouchEvent(event);
        }

        public void onSharedPreferenceChanged(SharedPreferences prefs, String key)
        {
            Log.d(TAG, "onSharedPreferenceChanged(key='" + key + "')");

            String s;
            boolean b;

            // Draw Mode
            String match = "draw_mode";
            if (key == null || key.equals(match))
            {
                int drawMode = Model.DRAW_FILLED;
                s = prefs.getString(match, "filled");
                if (s.equals("filled"))
                {
                    drawMode = Model.DRAW_FILLED;
                }
                else if (s.equals("wireframe"))
                {
                    drawMode = Model.DRAW_WIREFRAME;
                }
                else if (s.equals("wireframe_hlr"))
                {
                    drawMode = Model.DRAW_WIREFRAME_HIDDEN_LINE;
                }
                Log.d(TAG, "Setting draw_mode " + drawMode);
                mRenderer.setDrawMode(drawMode);
            }

            // Random Ships
            match = "random_ships";
            if (key == null || key.equals(match))
            {
                b = prefs.getBoolean(match, false);
                Log.d(TAG, "Setting random_ships " + b);
                mRenderer.setRandomiseShipModel(b);
            }
        }
    }
}

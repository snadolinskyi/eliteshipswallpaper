/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Coriolis extends Model
{
    public Coriolis()
    {
        super("Coriolis");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.175187f,
                0.267923f, 0.082451f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.001", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.432697f,
                0.709540f, 0.155853f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(4, 0, 3, 0, 0, 0, mMaterials[0]), new Face(0, 5, 1, 1, 1, 1, mMaterials[0]),
            new Face(1, 6, 2, 2, 2, 2, mMaterials[0]), new Face(2, 7, 3, 3, 3, 3, mMaterials[0]),
            new Face(4, 3, 7, 4, 4, 4, mMaterials[2]), new Face(4, 7, 8, 4, 4, 4, mMaterials[2]),
            new Face(5, 0, 9, 5, 5, 5, mMaterials[2]), new Face(9, 0, 4, 5, 5, 5, mMaterials[2]),
            new Face(10, 7, 2, 6, 6, 6, mMaterials[2]), new Face(10, 2, 6, 6, 6, 6, mMaterials[2]),
            new Face(1, 5, 11, 7, 7, 7, mMaterials[2]), new Face(1, 11, 6, 7, 7, 7, mMaterials[2]),
            new Face(8, 7, 10, 8, 8, 8, mMaterials[0]), new Face(8, 9, 4, 9, 9, 9, mMaterials[0]),
            new Face(11, 5, 9, 10, 10, 10, mMaterials[0]), new Face(10, 6, 11, 11, 11, 11, mMaterials[0]),
            new Face(8, 10, 11, 12, 12, 12, mMaterials[2]), new Face(8, 11, 9, 12, 12, 12, mMaterials[2]),
            new Face(12, 13, 14, 13, 13, 13, mMaterials[1]), new Face(12, 14, 15, 13, 13, 13, mMaterials[1]),
            new Face(1, 2, 15, 14, 14, 14, mMaterials[2]), new Face(2, 12, 15, 15, 15, 15, mMaterials[2]),
            new Face(2, 3, 12, 16, 16, 16, mMaterials[2]), new Face(3, 13, 12, 17, 17, 17, mMaterials[2]),
            new Face(1, 15, 14, 18, 18, 18, mMaterials[2]), new Face(0, 1, 14, 19, 19, 19, mMaterials[2]),
            new Face(0, 14, 13, 20, 20, 20, mMaterials[2]), new Face(0, 13, 3, 21, 21, 21, mMaterials[2])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(15.999000031249f, -15.999000031249f, 0f),
            new Vector3f(15.999000031249f, 0.000499984375488266f, -15.9995000156245f),
            new Vector3f(15.999000031249f, 16f, -0f),
            new Vector3f(15.999000031249f, 0.000499984375488266f, 15.9995000156245f),
            new Vector3f(-0.000499984375488266f, -15.999000031249f, 15.9995000156245f),
            new Vector3f(-0.000499984375488266f, -15.999000031249f, -15.9995000156245f),
            new Vector3f(-0.000499984375488266f, 16f, -15.9995000156245f),
            new Vector3f(-0.000499984375488266f, 16f, 15.9995000156245f),
            new Vector3f(-16f, 0.000499984375488266f, 15.9995000156245f), new Vector3f(-16f, -15.999000031249f, 0f),
            new Vector3f(-16f, 16f, -0f), new Vector3f(-16f, 0.000499984375488266f, -15.9995000156245f),
            new Vector3f(16f, 1.00046873535202f, 2.9999062529296f),
            new Vector3f(16f, -0.999468766601044f, 2.9999062529296f),
            new Vector3f(16f, -0.999468766601044f, -2.9999062529296f),
            new Vector3f(16f, 1.00046873535202f, -2.9999062529296f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0.57735f, -0.57735f, 0.57735f), new Vector3f(0.57735f, -0.57735f, -0.57735f),
            new Vector3f(0.57735f, 0.57735f, -0.57735f), new Vector3f(0.57735f, 0.57735f, 0.57735f),
            new Vector3f(0f, 0f, 1f), new Vector3f(0f, -1f, 0f), new Vector3f(0f, 1f, -0f),
            new Vector3f(-0f, -0f, -1f), new Vector3f(-0.57735f, 0.57735f, 0.57735f),
            new Vector3f(-0.57735f, -0.57735f, 0.57735f), new Vector3f(-0.57735f, -0.57735f, -0.57735f),
            new Vector3f(-0.57735f, 0.57735f, -0.57735f), new Vector3f(-1f, 0f, 0f), new Vector3f(1f, 0f, 0f),
            new Vector3f(1f, 8.3e-005f, -8.3e-005f), new Vector3f(1f, 6.7e-005f, -0f),
            new Vector3f(1f, 8.3e-005f, 8.3e-005f), new Vector3f(1f, 0f, 7.7e-005f), new Vector3f(1f, 0f, -7.7e-005f),
            new Vector3f(1f, -8.3e-005f, -8.3e-005f), new Vector3f(1f, -6.7e-005f, 0f),
            new Vector3f(1f, -8.3e-005f, 8.3e-005f)
        };
        return narr;
    }
}

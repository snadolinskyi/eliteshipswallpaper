/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Moray extends Model
{
    public Moray()
    {
        super("Moray");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f, 0.000000f,
                0.480000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.551566f, 0.800000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("LightBlue", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.202408f, 0.202408f,
                0.794105f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.350282f, 0.749017f,
                0.085349f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[0]), new Face(2, 1, 3, 1, 1, 1, mMaterials[2]),
            new Face(1, 0, 4, 2, 2, 2, mMaterials[2]), new Face(3, 1, 5, 3, 3, 3, mMaterials[0]),
            new Face(5, 1, 6, 4, 4, 4, mMaterials[0]), new Face(6, 1, 4, 5, 5, 5, mMaterials[0]),
            new Face(5, 2, 3, 6, 6, 6, mMaterials[0]), new Face(6, 0, 5, 7, 7, 7, mMaterials[2]),
            new Face(5, 0, 2, 7, 7, 7, mMaterials[2]), new Face(4, 0, 6, 8, 8, 8, mMaterials[0]),
            new Face(7, 8, 9, 9, 9, 9, mMaterials[1]), new Face(10, 11, 12, 10, 10, 10, mMaterials[3]),
            new Face(10, 12, 13, 10, 10, 10, mMaterials[3]), new Face(14, 15, 16, 10, 10, 10, mMaterials[3]),
            new Face(14, 16, 17, 10, 10, 10, mMaterials[3])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(4f, 0f, 17.3333333333333f), new Vector3f(0f, 4.8f, -10.6666666666667f),
            new Vector3f(-4f, 0f, 17.3333333333333f), new Vector3f(-16f, 0f, 0f), new Vector3f(16f, 0f, 0f),
            new Vector3f(-8f, -7.2f, -2.66666666666667f), new Vector3f(8f, -7.2f, -2.66666666666667f),
            new Vector3f(2.4f, -1.09333333333333f, -6.74666666666667f),
            new Vector3f(0f, -4.82666666666667f, -4.34666666666667f),
            new Vector3f(-2.4f, -1.09333333333333f, -6.74666666666667f),
            new Vector3f(3.2f, 0.826666666666667f, 13.0666666666667f),
            new Vector3f(1.33333333333333f, 0f, 17.3333333333333f), new Vector3f(1.6f, 0f, 17.3333333333333f),
            new Vector3f(3.46666666666667f, 0.826666666666667f, 13.0666666666667f),
            new Vector3f(-3.46666666666667f, 0.826666666666667f, 13.0666666666667f),
            new Vector3f(-1.6f, 0f, 17.3333333333333f), new Vector3f(-1.33333333333333f, 0f, 17.3333333333333f),
            new Vector3f(-3.2f, 0.826666666666667f, 13.0666666666667f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, 0.985622f, 0.168964f), new Vector3f(-0.199151f, 0.970222f, 0.137874f),
            new Vector3f(0.199151f, 0.970222f, 0.137874f), new Vector3f(-0.489965f, -0.233316f, -0.839939f),
            new Vector3f(0f, -0.5547f, -0.83205f), new Vector3f(0.489965f, -0.233316f, -0.839939f),
            new Vector3f(-0.546407f, -0.747223f, 0.378282f), new Vector3f(0f, -0.940887f, 0.338719f),
            new Vector3f(0.546407f, -0.747223f, 0.378282f), new Vector3f(0f, -0.540758f, -0.841178f),
            new Vector3f(0f, 0.981743f, 0.190213f)
        };
        return narr;
    }
}
